using System;
using System.Threading.Tasks;
using Xunit;

namespace CashCard.Tests
{
    public class CashCardTests
    {
        [Fact]
        public void CreateCard_WithInitialBalance_SetsBalance()
        {
            var initialBalance = 1000;
            var cashCard = new CashCard(1234, initialBalance);

            Assert.Equal(initialBalance, cashCard.Balance());
        }

        [Fact]
        public async Task TopUp_AddsTopUpTransaction()
        {
            var cashCard = new CashCard(1234, 0);
            await cashCard.TopUp(750);

            Assert.Single(cashCard.Transactions);

            var transaction = cashCard.Transactions[0];
            Assert.Equal(TransactionType.Topup, transaction.Type);
        }

        [Fact]
        public async Task TopUp_UpdatesBalance()
        {
            var initialBalance = 1500;
            var cashCard = new CashCard(1234, initialBalance);
            await cashCard.TopUp(600);

            Assert.Equal(2100, cashCard.Balance());
        }

        [Fact]
        public void TopUp_CanProcessConcurrentTopups()
        {
            var cashCard = new CashCard(1234, 0);
            var tasks = new Task[1000];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Run(async () => await cashCard.TopUp(1000));
            }
            Task.WaitAll(tasks);

            Assert.Equal(1000000, cashCard.Balance());
        }

        [Fact]
        public async Task Withdraw_WithIncorrectPin_DoesNotUpdateBalance()
        {
            var pinNumber = 1234;
            var initialBalance = 1000;
            var cashCard = new CashCard(pinNumber, initialBalance);

            await cashCard.Withdraw(pinNumber: 9999, amount: 500);

            Assert.Equal(initialBalance, cashCard.Balance());
        }

        [Fact]
        public async Task Withdraw_WithCorrectPin_AddsWithdrawalTransaction()
        {
            var cashCard = new CashCard(1234, 1000);
            await cashCard.Withdraw(1234, 750);

            Assert.Single(cashCard.Transactions);

            var transaction = cashCard.Transactions[0];
            Assert.Equal(TransactionType.Withdrawal, transaction.Type);
        }

        [Fact]
        public async Task Withdraw_WithCorrectPin_UpdatesBalance()
        {
            var initialBalance = 1500;
            var cashCard = new CashCard(1234, initialBalance);
            await cashCard.Withdraw(1234, 600);

            Assert.Equal(900, cashCard.Balance());
        }

        [Fact]
        public async Task Withdraw_AmountGreaterThanBalance_DoesNotUpdateBalance()
        {
            var initialBalance = 1000;
            var withdrawalAmount = 1500;
            var cashCard = new CashCard(1234, initialBalance);
            await cashCard.Withdraw(1234, withdrawalAmount);

            Assert.Equal(initialBalance, cashCard.Balance());
        }

        [Fact]
        public void Withdraw_CanProcessConcurrentWithdrawals()
        {
            var initialBalance = 1000000;
            var cashCard = new CashCard(1234, initialBalance);
            var tasks = new Task[1000];
            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Run(async () => await cashCard.Withdraw(1234, 500));
            }
            Task.WaitAll(tasks);

            Assert.Equal(500000, cashCard.Balance());
        }

        [Fact]
        public async Task Balance_ReturnsInitialBalancePlusTransactionsAmountTotal()
        {
            var pinNumber = 1234;
            var initialBalance = 175;
            var cashCard = new CashCard(pinNumber, initialBalance);

            await cashCard.TopUp(750);
            await cashCard.Withdraw(pinNumber, 25);
            await cashCard.TopUp(340);
            await cashCard.TopUp(750);
            await cashCard.Withdraw(pinNumber, 25);

            Assert.Equal(1965, cashCard.Balance());
        }
    }
}
