﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CashCard
{
    public class CashCard : ICashCard
    {
        private readonly int _pinNumber;
        private readonly decimal _initialBalance;
        private readonly object _transactionLock = new object();

        public CashCard(int pinNumber, decimal initialBalance)
        {
            _pinNumber = pinNumber;
            _initialBalance = initialBalance;
            Transactions = new List<Transaction>();
        }

        public decimal Balance()
        {
            return _initialBalance + GetTotalTopUps() - GetTotalWithdrawals();
        }

        public async Task TopUp(decimal amount)
        {
            lock (_transactionLock)
            {
                Transactions.Add(new Transaction(TransactionType.Topup, amount));
            }
        }

        public async Task Withdraw(int pinNumber, int amount)
        {
            lock (_transactionLock)
            {
                if (pinNumber != _pinNumber || amount > Balance()) return;

                Transactions.Add(new Transaction(TransactionType.Withdrawal, amount));
            }
        }

        private decimal GetTotalTopUps()
        {
            return Transactions.Where(x => x.Type == TransactionType.Topup).Sum(y => y.Amount);
        }

        private decimal GetTotalWithdrawals()
        {
            return Transactions.Where(x => x.Type == TransactionType.Withdrawal).Sum(y => y.Amount);
        }

        public List<Transaction> Transactions { get; private set; }
    }
}
