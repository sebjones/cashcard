﻿using System.Threading.Tasks;

namespace CashCard
{
    public interface ICashCard
    {
        decimal Balance();
        Task TopUp(decimal amount);
        Task Withdraw(int pinNumber, int amount);
    }
}
