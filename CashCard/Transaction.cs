﻿using System;

namespace CashCard
{
    public class Transaction
    {
        public Transaction(TransactionType type, decimal amount)
        {
            Type = type;
            Amount = amount;
            DateTime = DateTime.Now;
        }

        public TransactionType Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime DateTime { get; set; }
    }

    public enum TransactionType
    {
        Topup,
        Withdrawal
    }
}
